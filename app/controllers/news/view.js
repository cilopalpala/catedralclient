'use strict';

angular.module('catedralApp.newsview', ['ngRoute','ui.tinymce','ngFileUpload','ngSanitize'])

.controller('NewsViewController', ['$scope', '$stateParams','DailyPrayerResource',function($scope,$stateParams,DailyPrayerResource) {

    var id = $stateParams.id;
    DailyPrayerResource.get({id:id},function(data){
        $scope.news = data.data;
        $scope.posts = "noticias";
        $("#newsDescription").append($scope.news.description);
        $scope.band = true;
    })

}]);