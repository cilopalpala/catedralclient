'use strict';

angular.module('catedralApp.news', ['ngRoute', 'ngResource','ngTable'])

.controller('NewsHomeController', ['$scope', '$route','DailyPrayerResource','NgTableParams','$http','filterFilter','$routeParams','$location','$state',function($scope,$route,DailyPrayerResource,NgTableParams,$http,filterFilter,$routeParams, $location,$state){

    $scope.daily_prayers = [];
    $scope.posts = [];
    $scope.postsFilter = [];
    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.filtered = [];
    $scope.search = '';

    $http.post('https://catedraldelafe.herokuapp.com/api/posts/list?type=3').success(function(response) { 
                $scope.posts = response.data.data;
                $scope.postsFilter = response.data.data;
                $scope.setValuePagination(response);
    }).error(function (response) {
         if(response.error = "token_not_provided"){
            alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
            $scope.logout();
         } 
    });

    var params = {
            page: 1,
            count: 10
        };


    $scope.getPosts = function(pageNumber){
        var url = '';
        if($scope.bandTitle){
            //Post.get({page: pageNumber},function(response){
            $http.get('https://catedraldelafe.herokuapp.com/api/posts/searchByTitle?type=3&title='+$scope.search+'&page='+pageNumber).success(function(response) { 
              $scope.posts = response.data.data;
              $scope.setValuePagination(response);

            }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
            });
        }else{
            $http.post('https://catedraldelafe.herokuapp.com/api/posts/list?type=3&page='+pageNumber).success(function(response) { 
              $scope.posts = response.data.data;
              $scope.setValuePagination(response);

            }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
            });
        }
        if(pageNumber===undefined){
          pageNumber = '1';
        }
        

    };


    var settings = {
        total: 0,
        getData: function($defer, params) {

            /*DailyPrayerResource.query({type:2},function(response) {
                console.log(response.data.total);
                //params.total(response.data.total);
                //$defer.resolve(response.data.data);
            });*/
        }
    };

    //$scope.tableParams = new NgTableParams(params, settings);

    $scope.removePrayer = function(id){
        $http.delete('https://catedraldelafe.herokuapp.com/api/posts/'+id).success(function(response) {    
            if(response.message){
                $state.reload();
            }
        }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
        });        
    }

    $scope.searchTitle = function () {
         console.log('Entrando por le keypress');
         //if(String($scope.search).length > 1){
            $http.get('https://catedraldelafe.herokuapp.com/api/posts/searchByTitle?type=3&title='+$scope.search).success(function(response) { 
              $scope.posts = response.data.data;
              $scope.setValuePagination(response);
              $scope.bandTitle = true;
            }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
            });
         //}
    }
    //$scope.search = {};

    /*$scope.$watch('search', function (newVal, oldVal) {
        $scope.filtered = filterFilter($scope.postsFilter, newVal);
        console.log("Esto es lo que tiene el filtered ->",$scope.filtered)
        $scope.currentPage = 1;
        $scope.totalItems = $scope.filtered.length;
        $scope.totalPages = Math.ceil($scope.totalItems / $scope.entryLimit);
    }, true);*/
	

    $scope.setValuePagination = function(response){
        $scope.totalPages = response.data.last_page;
        $scope.currentPage  = response.data.current_page;
        var pages = [];
        for(var i=1;i<=response.data.last_page;i++) {          
            pages.push(i);
        }
        $scope.entryLimit = response.data.per_page;
        $scope.totalItems = response.data.total;
        $scope.numPages = response.data.last_page;
        $scope.range = pages; 
    }
}])