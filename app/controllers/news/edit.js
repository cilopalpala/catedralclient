'use strict';

angular.module('catedralApp.newsedit', ['ngRoute','ui.tinymce','ngFileUpload'])

.controller('NewsEditController', ['$scope', '$stateParams', '$http', '$location', 'DailyPrayerResource', 'Upload','$compile',function($scope, $stateParams, $http, $location, DailyPrayerResource,Upload,$compile) {

    $scope.settings = {
        action: 'Editar'
    }
    $scope.news_edit = true;
    $scope.news = true;
    $scope.link = '';
    $scope.v_index = 0;
    $scope.videos = [];

    var id = $stateParams.id;
    DailyPrayerResource.get({id:id},function(data){
        $scope.prayer = data.data;
        var images = [];
        for (var i = 0; i < data.data.images.length; i++) {
            var im={};
            im['id'] = data.data.images[i].id;
            im['url'] = "https://catedraldelafe.herokuapp.com/"+data.data.images[i].url;
        	images.push(im);
        };
        $scope.prayer.imagesCreted = images;
        $scope.prayer.edit=true;
        $scope.prayer.images = '';

    })

    $scope.removeItem = function(index){
        $scope.prayer.images.splice(index, 1);
    }

    $scope.deleteImage = function(id){
        $http.delete('https://catedraldelafe.herokuapp.com/api/images/delete/'+id).then( function successCallback(response){
            angular.element('#img_'+id).parent().remove();
        }, function errorCallback(response){
        });
    }

    $scope.deleteVideo = function(event,aux){
        var aux_url = $scope.videos.indexOf($(".video_"+aux+"").data('video').toString());
        if(aux_url !== -1){
            $scope.videos.splice(aux_url,1);
            $(".video_"+aux+"").remove();
        }
    };

    $scope.deleteVideoEdit = function(id){
        $http.delete('https://catedraldelafe.herokuapp.com/api/videos/delete/'+id).then( function successCallback(response){
            angular.element('#v_'+id).parent().remove();
        }, function errorCallback(response){
        });
    }

    $scope.addVideo = function(){
        var aux = $scope.v_index + 1;
        $scope.v_index = aux;
        $scope.youtubeid = $("#video_link").val().replace("watch?v=", "v/");
        $scope.videos.push($scope.youtubeid);
        $scope.video_index = $scope.videos.indexOf($scope.youtubeid);
        var my_video = '<div class="col-md-6 videoClass video_'+aux+'" data-video="'+$scope.youtubeid+'">'+
                            '<div class="embed-responsive embed-responsive-16by9">'+
                                '<iframe class="embed-responsive-item" src="'+$scope.youtubeid+'" frameborder="0">'+
                                '</iframe>'+
                            '</div>'+
                            '<div class="hover-btn">'+
                                '<button type="button" class="btn btn-danger" data-dismiss="alert" ng-click="deleteVideo($event,'+aux+')" ><i class="fa fa-times" ></i></button>'+
                            '</div>'+
                        '</div>';
        angular.element(document.getElementById('video_content')).append($compile(my_video)($scope));

        $("#video_link").val('');
    };

    $scope.submit = function() {
        console.log("prayer",$scope.prayer);
        $scope.uploadFiles($scope.prayer.images);
    };

    $scope.uploadFiles = function (images) {
        Upload.upload({
            url: 'https://catedraldelafe.herokuapp.com/api/posts/update/'+$scope.prayer.id,
            method: 'POST',
            data: {'title': $scope.prayer.title, 'description':$scope.prayer.description,'type_id':3,'post_images': images,'post_videos': $scope.videos},
            headers: {"Content-type": undefined},
        }).then(function (resp) {
            if(resp.data.message){
                $location.url("/noticias")
            }
        }, function (resp) {
            if(resp.data.error == "token_not_provided"){
                alert("El tiempo se sesión ha caducado. Por favor, ingrese de nuevo al sistema");
                $scope.logout();
            }
            if(resp.data.errors){
                $scope.settings.errors = resp.data.errors[0].message;
            }
        });
    }
}])

