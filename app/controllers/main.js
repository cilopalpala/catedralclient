//require('app/assets/dist/js/app.js');
'use strict';
/* App Module */
var catedralApp = angular.module('catedralApp', [
  'ngRoute',
  'ui.router',
  'ui.bootstrap',
//  'catedralApp.auth',
  'satellizer',
  'ngCookies',
  'permission',
//  'catedralApp.home',
  'catedralApp.news',
  'catedralApp.newsview',
  'catedralApp.newsadd',
  'catedralApp.newsedit',
  'catedralApp.dailyprayer',
  'catedralApp.dailyprayerview',
  'catedralApp.dailyprayeradd',
  'catedralApp.dailyprayeredit',
  'catedralApp.events',
  'catedralApp.newsDirective',
  'catedralApp.auth',
  'catedralApp.authHome',
  'catedralApp.authEdit',
  'catedralApp.authFilter',
  'catedralApp.postsPaginationDirective',
  'catedralApp.postsFilter',
  'catedralApp.sermonsemanal',
  'catedralApp.sermonsemanalview',
  'catedralApp.sermonsemanaledit',
  'catedralApp.sermonsemanaladd'
])

catedralApp.config(function($stateProvider,$urlRouterProvider, $authProvider, $httpProvider, $provide,$sceDelegateProvider) {

            // Satellizer configuration that specifies which API
            // route the JWT should be retrieved from
            $authProvider.loginUrl = 'https://catedraldelafe.herokuapp.com/api/authenticate';
            //$authProvider.loginUrl = 'http://localhost:8000/api/authenticate';
            // Redirect to the auth state if any other states
            // are requested other than users
            $urlRouterProvider.otherwise('/home');

            $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login/login.html',
                    controller: 'AuthController'
                })
                .state('home', {
                    url: '/home',
                    templateUrl: 'views/home/home.html',
                    controller: 'MainCtrl'
                })
                .state('register', {
                    url: '/registrarse',
                    templateUrl: 'views/login/register.html',
                    controller: 'AuthController',
                    data: {
                      permissions: {
                        only: ['admin'],
                        redirectTo: function(){
                            alert("Usted no tiene permisos para ingresar a esta sección.")
                            return 'home';
                        }
                      }
                    }
                })
                .state('users', {
                    url: '/usuarios',
                    templateUrl: 'views/login/home.html',
                    controller: 'AuthHomeController',
                    data: {
                      permissions: {
                        only: ['admin'],
                        redirectTo: function(){
                            alert("Usted no tiene permisos para ingresar a esta sección.")
                            return 'home';
                        }
                      }
                    }
                })
                .state('usersEdit', {
                    url: '/usuarios/editar/:id',
                    templateUrl: 'views/login/edit.html',
                    controller: 'AuthEditController',
                    /*data: {
                      permissions: {
                        only: ['admin'],
                        redirectTo: function(){
                            alert("Usted no tiene permisos para ingresar a esta sección.")
                            return 'home';
                        }
                      }
                    }*/
                })
                .state('reflexiondiaria', {
                    url: '/reflexiondiaria',
                    templateUrl: 'views/daily_prayer/home.html',
                    controller: 'DailyPrayerHomeController'
                })
                .state('reflexiondiariaView', {
                    url: '/reflexiondiaria/view/:id',
                    templateUrl: 'views/daily_prayer/view.html',
                    controller: 'DailyPrayerViewController'
                })
                .state('reflexiondiariaNueva', {
                    url: '/reflexiondiaria/nueva',
                    templateUrl: 'views/daily_prayer/add.html',
                    controller: 'DailyPrayerAddController'
                })
                .state('reflexiondiariaEditar', {
                    url: '/reflexiondiaria/editar/:id',
                    templateUrl: 'views/daily_prayer/edit.html',
                    controller: 'DailyPrayerEditController'
                })
                .state('newsView', {
                    url: '/noticias/view/:id',
                    templateUrl: 'views/news/view.html',
                    controller: 'NewsViewController'
                })
                .state('newsAdd', {
                    url: '/noticias/nueva',
                    templateUrl: 'views/news/add.html',
                    controller: 'NewsAddController'
                })
                .state('newsEdit', {
                    url: '/noticias/editar/:id',
                    templateUrl: 'views/news/edit.html',
                    controller: 'NewsEditController'
                })
                .state('news', {
                    url: '/noticias',
                    templateUrl: 'views/news/home.html',
                    controller: 'NewsHomeController'
                })
                .state('eventos', {
                    url: '/eventos',
                    templateUrl: 'views/events/home.html',
                    controller: 'EventsController'
                })
                .state('sermonsemanal', {
                    url: '/sermonsemanal',
                    templateUrl: 'views/sermonsemanal/home.html',
                    controller: 'SermonSemanalHomeController'
                })
                .state('sermonsemanalAdd', {
                    url: '/sermonsemanal/nueva',
                    templateUrl: 'views/sermonsemanal/add.html',
                    controller: 'SermonSemanalAddController'
                })
                .state('sermonsemanalEdit', {
                    url: '/sermonsemanal/editar/:id',
                    templateUrl: 'views/sermonsemanal/edit.html',
                    controller: 'SermonSemanalEditController'
                })
                .state('sermonsemanalView', {
                    url: '/sermonsemanal/view/:id',
                    templateUrl: 'views/sermonsemanal/view.html',
                    controller: 'SermonSemanalViewController'
                });





            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain.  Notice the difference between * and **.
                '*://www.youtube.com/**'
              ]);
});

catedralApp.service('LoggedInStatus', [function(){
    var loggedIn = "";
    return {
        getStatus: function () {
            return loggedIn;
        },
        setStatus: function (value) {
            loggedIn = value;
        }
    };
}]);

catedralApp.run(function(PermissionStore,$rootScope, $state,$q) {
            $rootScope.authenticated = false;
            $rootScope.bandLib = true;

            $rootScope.$on('$stateChangeStart', function(event, toState) {
                // Grab the user from local storage and parse it to an object
                var user = JSON.parse(localStorage.getItem('user'));
                if(user) {
                    $rootScope.authenticated = true;
                    //$rootScope.bandLib = true;
                    $rootScope.currentUser = user;
                    if(toState.name === "auth") {
                        event.preventDefault();
                        $state.go('home');
                    }
                }
            });

            PermissionStore.definePermission('admin', function (stateParams) {
                var deferred = $q.defer();
                if($rootScope.currentUser){
                    if($rootScope.currentUser.role_id == 1){
                    deferred.resolve();
                    }else{
                        deferred.reject();
                    }
                }
                return deferred.promise;
            });
});

catedralApp.controller('MainCtrl', ['$auth','$state','$scope', '$location','$rootScope','$http','LoggedInStatus','$cookieStore','$route', function ($auth, $state, $scope, $location,$rootScope,$http,loggedInStatus,$cookieStore,$route) {
    $scope.loggedIn = false;

    //location.reload();
    //$state.go($state.current.name, $state.params, { reload: true });
    $scope.reloadApp = function(){
        /*if($rootScope.bandLib){
                $.getScript('assets/dist/js/app.js');
                $rootScope.bandLib = false;
                //console.log('el script esta cargado.');
        }*/
    }

    $scope.getStateUser = function(){
        $scope.loggedIn = $cookieStore.get('loggedin');
        //$rootScope.bandLib = true;
    }
    $scope.loggedIn = $cookieStore.get('loggedin');
    $scope.loggON = loggedInStatus.getStatus();

    $scope.logout = function() {

            $auth.logout().then(function() {
                localStorage.removeItem('user');

                loggedInStatus.setStatus(false);

                $cookieStore.put('loggedin', false);

                $rootScope.authenticated = false;

                $rootScope.currentUser = null;

                $state.go('login');
            });
    }

    $scope.addClassAct = function(id) {
        $(".nav_menu").removeClass("active");
        $("#"+id+"").addClass("active");
    }

    /*$scope.$on('$stateChangeStart', function(event, toState) {
        
        
    });*/
    $rootScope.$on('$stateChangeStart', 
        function(event, toState, toParams, fromState, fromParams){ 
            if(!$scope.authenticated){
                $location.path('/login');
            }

            var len = $('script').filter(function () {
                return ($(this).attr('src') == 'assets/dist/js/app.js');
            }).length;

            if($rootScope.bandLib){
                $.getScript('assets/dist/js/app.js');
                $rootScope.bandLib = false;
                console.log('el script esta cargado.');
            }
    });

}]);


catedralApp.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);
