'use strict';

angular.module('catedralApp.dailyprayeradd', ['ngRoute','ui.tinymce','ngFileUpload','mwl.calendar'])

.controller('DailyPrayerAddController', ['$scope', 'Upload','$location',function($scope,Upload,$location) {

    $scope.tinymceOptions = {
        language: "es"
    };
    $scope.settings = {
        action: 'Guardar',
        home: 'reflexiondiaria'
    }

    $scope.prayer = {
        title :'',
        description :'',
        images :'',
        imagesExist: '',
        edit: false,
        daily: true
    }

/*** scopes para configurar el calendario ***/
    $scope.calendarView = 'month';
    $scope.viewDate = moment().startOf('month').toDate();
    $scope.isCellOpen = true;
    $scope.viewChangeEnabled = true;
    $scope.popup1 = {
      opened: false
    };

    $scope.removeItem = function(index){
        $scope.prayer.images.splice(index, 1);
    }
     
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.submit = function(formData) {
        $scope.uploadFiles($scope.prayer.images);
    };

    $scope.uploadFiles = function (images) {
        Upload.upload({
            url: 'https://catedraldelafe.herokuapp.com/api/posts/store',
            data: {'title': $scope.prayer.title, 'description':$scope.prayer.description,'date':Date.parse($scope.prayer.date),'type_id':1,'post_images': images},
            headers: {"Content-type": undefined},
        }).then(function (resp) {
            if(resp.status == 201){
                $location.path('/reflexiondiaria/view/'+resp.data.data.id);  
            }
            if(resp.data.message){
                $scope.settings.success = resp.data.message;
                angular.copy({},$scope.prayer);
                $scope.dailyPrayerForm.$setUntouched();
            }
        }, function (resp) {
            //console.log("esto es el error ->",resp);
            if(resp.data.error == "token_not_provided"){
                alert("El tiempo se sesión ha caducado. Por favor, ingrese de nuevo al sistema");
                $scope.logout();
            }
            if(resp.data.errors){
                $scope.settings.errors = resp.data.errors[0].message;
            }
        });
    }
}])
