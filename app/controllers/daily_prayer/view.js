'use strict';

angular.module('catedralApp.dailyprayerview', ['ngRoute','ui.tinymce','ngFileUpload','ngSanitize'])

.controller('DailyPrayerViewController', ['$scope', '$stateParams','DailyPrayerResource',function($scope,$stateParams,DailyPrayerResource) {

    var id = $stateParams.id;
    DailyPrayerResource.get({id:id},function(data){
        $scope.news = data.data;
        $scope.posts = "reflexiondiaria";
        $("#newsDescription").append($scope.news.description)
        $scope.band = false;
    })

}]);