'use strict';

angular.module('catedralApp.dailyprayeredit', ['ngRoute','ui.tinymce','ngFileUpload'])

.controller('DailyPrayerEditController', ['$scope', '$stateParams', '$http', '$location', 'DailyPrayerResource', 'Upload',function($scope, $stateParams, $http, $location, DailyPrayerResource,Upload) {

    $scope.settings = {
        action: 'Editar'
    }
    var id = $stateParams.id;
    DailyPrayerResource.get({id:id},function(data){
        $scope.prayer = data.data;
        var images = [];
        for (var i = 0; i < data.data.images.length; i++) {
            var im={};
            im['id'] = data.data.images[i].id;
            im['url'] = "https://catedraldelafe.herokuapp.com/"+data.data.images[i].url;
        	images.push(im);
        };
        $scope.prayer.imagesCreted = images;
        $scope.prayer.edit=true;
        $scope.prayer.images = '';

    })

    $scope.removeItem = function(index){
        $scope.prayer.images.splice(index, 1);
    }

    $scope.deleteImage = function(id){
        $http.delete('https://catedraldelafe.herokuapp.com/api/images/delete/'+id).then( function successCallback(response){
            angular.element('#img'+id).parent().remove();
        }, function errorCallback(response){
        });
    }

    $scope.submit = function() {
        console.log("prayer",$scope.prayer);
        $scope.uploadFiles($scope.prayer.images);
    };

    $scope.uploadFiles = function (images) {
        Upload.upload({
            url: 'https://catedraldelafe.herokuapp.com/api/posts/update/'+$scope.prayer.id,
            method: 'POST',
            data: {'title': $scope.prayer.title, 'description':$scope.prayer.description,'type_id':1,'post_images': images},
            headers: {"Content-type": undefined},
        }).then(function (resp) {
            if(resp.data.message){
                $location.url("/reflexiondiaria")
            }
            console.log('Entrando por le exito ->',resp);
        }, function (resp) {
            if(resp.data.error == "token_not_provided"){
                alert("El tiempo se sesión ha caducado. Por favor, ingrese de nuevo al sistema");
                $scope.logout();
            }
            if(resp.data.errors){
                $scope.settings.errors = resp.data.errors[0].message;
            }
        });
    }
}])

