'use strict';

angular.module('catedralApp.dailyprayer', ['ngRoute', 'ngResource','ngTable'])

.controller('DailyPrayerHomeController', ['$scope', '$route','DailyPrayerResource','NgTableParams','$http','filterFilter','$state','$location',function($scope,$route,DailyPrayerResource,NgTableParams,$http,filterFilter,$state,$location){

    $scope.daily_prayers = [];
    $scope.posts = [];
    $scope.postsFilter = [];
    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.filtered = [];
    $scope.search = '';

    $http.post('https://catedraldelafe.herokuapp.com/api/posts/list?type=1').success(function(response) { 
                $scope.posts = response.data.data;
                $scope.postsFilter = response.data.data;
                $scope.setValuePagination(response);
    }).error(function (response) {
         //console.log('esta es la respuesta con error ->',response);
         if(response.error = "token_not_provided"){
            alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
            $scope.logout();
            //$location.url("/login");
         } 
    });

    var params = {
            page: 1,
            count: 10
        };


    $scope.getPosts = function(pageNumber){
        var url = '';
        if($scope.bandTitle){
            //Post.get({page: pageNumber},function(response){
            $http.get('https://catedraldelafe.herokuapp.com/api/posts/searchByTitle?type=1&title='+$scope.search+'&page='+pageNumber).success(function(response) { 
              $scope.posts = response.data.data;
              $scope.setValuePagination(response);

            }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
            });
        }else{
            console.log('Entrando por el falso');
            $http.post('https://catedraldelafe.herokuapp.com/api/posts/list?type=1&page='+pageNumber).success(function(response) { 
              $scope.posts = response.data.data;
              $scope.setValuePagination(response);

            }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
            });
        }
        if(pageNumber===undefined){
          pageNumber = '1';
        }
        

    };


    var settings = {
        total: 0,
        getData: function($defer, params) {
        }
    };

    //$scope.tableParams = new NgTableParams(params, settings);

    $scope.removePrayer = function(id){
        DailyPrayerResource.delete({id:id}).$promise.then(function(data){
            if(data.message){
                $state.reload();
            }
        });
    }

    $scope.searchTitle = function () {
         //if(String($scope.search).length > 1){
            $http.get('https://catedraldelafe.herokuapp.com/api/posts/searchByTitle?type=1&title='+$scope.search).success(function(response) { 
              $scope.posts = response.data.data;
              $scope.setValuePagination(response);
              $scope.bandTitle = true;
            }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
            });
         //}
    }
	

    $scope.setValuePagination = function(response){
        $scope.totalPages = response.data.last_page;
        $scope.currentPage  = response.data.current_page;
        var pages = [];
        for(var i=1;i<=response.data.last_page;i++) {          
            pages.push(i);
        }
        $scope.entryLimit = response.data.per_page;
        $scope.totalItems = response.data.total;
        $scope.numPages = response.data.last_page;
        $scope.range = pages; 
    }
}])