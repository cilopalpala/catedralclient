'use strict';

angular.module('catedralApp.authEdit', ['ngRoute','ui.tinymce','ngFileUpload'])

.controller('AuthEditController', ['$scope', '$stateParams', '$http', '$location', 'DailyPrayerResource', 'Upload','$compile',function($scope, $stateParams, $http, $location, DailyPrayerResource,Upload,$compile) {

    $scope.settings = {
        action: 'Editar'
    }

    var id = $stateParams.id; 
    $http.get('https://catedraldelafe.herokuapp.com/api/authenticate/'+id).then( function successCallback(response){
            if((id !== $scope.currentUser.id) && ($scope.currentUser.role_id == 2)){
                alert("Usted no tiene permitida la edicion de otros usuarios")
                $location.path('/home');
            }
            $scope.newUser = response.data.data;
        }, function errorCallback(response){
    });    


    $scope.editUser = function(){
        $http({
          method: 'POST',
          url: 'https://catedraldelafe.herokuapp.com/api/authenticate/update/'+$scope.newUser.id,
          data : $scope.newUser
        }).then(function successCallback(response) {
            console.log(response);
            if(response.status == 200){
                $location.url("/usuarios")
            }
        }, function errorCallback(response) {
        
        });    
    }
    

    $scope.submit = function() {
        console.log("prayer",$scope.prayer);
        $scope.uploadFiles($scope.prayer.images);
    };

   
}])

