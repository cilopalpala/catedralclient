'use strict';

angular.module('catedralApp.auth', ['ngRoute'])

.controller('AuthController',['$auth','$state','$scope', '$location','$rootScope','$http','LoggedInStatus','$cookieStore', function($auth, $state, $scope, $location,$rootScope,$http,loggedInStatus,$cookieStore) {
	
    $scope.user = {};
    $scope.newUser = {};
    $scope.newUser.role_id = "2";

    $scope.login =  function() {

            var credentials = $scope.user;

            $auth.login(credentials).then(function() {

                return $http.get('https://catedraldelafe.herokuapp.com/api/authenticate/user',{headers: $auth.getToken()}).then(function(response) {

                    var user = JSON.stringify(response.data.user);
                    localStorage.setItem('user', user);
                    loggedInStatus.setStatus(true);
                    $cookieStore.put('loggedin', true);
                    $rootScope.authenticated = true;
                    $rootScope.bandLib = true;
                    $rootScope.currentUser = response.data.user;
                    $location.path('/home');
                });

            // Handle errors
            }, function(error) {
                console.log(error);
                $scope.loginError = true;
                $scope.loginErrorText = error.data.error;
            });
        }

    $scope.logout = function() {

            $auth.logout().then(function() {
                localStorage.removeItem('user');

                loggedInStatus.setStatus(false);
                
                $cookieStore.put('loggedin', false);
                $rootScope.authenticated = false;
                $rootScope.bandLib = true;
                $rootScope.currentUser = null;
                $state.go('auth', {});
                //$route.reload();
            });
    }

    $scope.register = function(){
        //var credentials = $scope.user;
        $http.post('https://catedraldelafe.herokuapp.com/api/register',$scope.newUser)
        //$http.post('http://localhost:8000/api/register',$scope.newUser)
                .success(function(data){
                    //$scope.user.email=$scope.newUser.email;
                    //$scope.user.password=$scope.newUser.password;
                    //$scope.login();
                    $location.url("/usuarios")
        })
    }

}]);
