'use strict';

angular.module('catedralApp.authHome', ['ngRoute', 'ngResource','ngTable'])

.controller('AuthHomeController', ['$scope','DailyPrayerResource','$http','filterFilter','$routeParams','$state','$location',function($scope,DailyPrayerResource,$http,filterFilter,$routeParams,$state,$location){

    $scope.daily_prayers = [];
    $scope.posts = [];
    $scope.postsFilter = [];
    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.filtered = [];
    $scope.search = '';

    $http.get('https://catedraldelafe.herokuapp.com/api/authenticate/index').success(function(response) { 
                //$defer.resolve(response.data.data);
                $scope.users = response.data.data;
                //$scope.postsFilter = response.data.data;
                $scope.setValuePagination(response);
    }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
    });

    var params = {
            page: 1,
            count: 10
        };


    $scope.getPosts = function(pageNumber){
        var url = '';
        $http.post('https://catedraldelafe.herokuapp.com/api/authenticate/users?page='+pageNumber).success(function(response) { 
          $scope.users = response.data.data;
          $scope.setValuePagination(response);

        }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
        });
        if(pageNumber===undefined){
          pageNumber = '1';
        }
        

    };


    //$scope.tableParams = new NgTableParams(params, settings);

    $scope.removeUser = function(id){
        $http.delete('https://catedraldelafe.herokuapp.com/api/authenticate/'+id).success(function(response) {    
            if(response.code == 200){
                //$location.url("/home");
                //$state.go($state.current, {}, {reload: true});
                //$state.reload()
                location.reload();
            }
        }).error(function (response) {
                 if(response.error = "token_not_provided"){
                    alert("El tiempo de sesión ah caducado. Por favor, ingrese nuevamente al sistema.");
                    $scope.logout();
                 } 
        });
    }

	

    $scope.setValuePagination = function(response){
        $scope.totalPages = response.data.last_page;
        $scope.currentPage  = response.data.current_page;
        var pages = [];
        for(var i=1;i<=response.data.last_page;i++) {          
            pages.push(i);
        }
        $scope.entryLimit = response.data.per_page;
        $scope.totalItems = response.data.total;
        $scope.numPages = response.data.last_page;
        $scope.range = pages; 
    }
}])

