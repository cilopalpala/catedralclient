'use strict';

angular.module('catedralApp.events', ['ngRoute', 'ngResource','mwl.calendar','ui.bootstrap','ngTagsInput'])

.controller('EventsController', ['$scope', '$route','$http', '$filter', '$location', '$anchorScroll',function($scope,$route,$http,$filter, $location, $anchorScroll){
	
    

    /******* AQUI DEBERIA TRAER TODOS LOS EVENTOS DE BD *********/
    $scope.events = [];

    $scope.newEvent = {
      title: '',
      tags: '',
      startsAt: '',
      endsAt: '',
      description:'',
      editable: false
    }

    /*** scopes para configurar el calendario ***/
    $scope.calendarView = 'month';
    $scope.viewDate = moment().startOf('month').toDate();
    $scope.isCellOpen = true;
    $scope.viewChangeEnabled = true;
    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };

    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };

    /******* Autocompletar tags *******/
    $scope.loadTags = function(query){
      return $http.get('https://catedraldelafe.herokuapp.com/api/tags/autocomplete?query=' + query);
    }

    /******* EVENTOS DEL CALENDARIO ********/

    /** evento que cambia las vistas al hacer click en alguna fecha **/
    $scope.viewChangeClicked = function(date, nextView) {
      console.log(date, nextView);
      return $scope.viewChangeEnabled;
    };

    /*** evento que detecta el click en la opcion de editar un event en el calendario ***/
    $scope.eventEdited = function(event) {
      $location.hash('formEvent');
      $anchorScroll();  
      $scope.newEvent = event;    
    };

    /*** evento que detecta el click en la opcion de eliminar un event en el calendario ***/
    $scope.eventDeleted = function(event) {
      var index = $filter('getById')($scope.events, event.id);
      $http.delete('https://catedraldelafe.herokuapp.com/api/events/delete/'+event.id)
      $scope.events.splice(index);
    };

    $scope.loadEvents = function(){
      $http.post('https://catedraldelafe.herokuapp.com/api/events/').
        then(function successCallback(response){
            var events = response.data.data;
            console.log(events);
            for(var i=0;i<events.length;i++){
              var tags = [];
              for (var j = 0; j < events[i].tags.length;j++) {
                var t = {"text":events[i].tags[j].name}
                tags.push(t)
              };
              var event = {
                id: events[i].id,
                title: events[i].title,
                type: 'info',
                tags: tags,
                description: events[i].description,
                startsAt: new Date(parseInt(events[i].startsAt)),
                endsAt: new Date(parseInt(events[i].endsAt)),
                draggable: true,
                resizable: true,
                editable: true,
                deletable: true  
              };
              $scope.events.push(event);   
            }
        }, function errorCallback(data){

        });      
    }

    $scope.addEvent = function(){
        var event = {
            title: $scope.newEvent.title,
            type: 'info',
            tags: $scope.newEvent.tags,
            description:$scope.newEvent.description,
            startsAt: Date.parse($scope.newEvent.startsAt),
            endsAt: Date.parse($scope.newEvent.endsAt),
            draggable: true,
            resizable: true,
            editable: true,
            deletable: true           
        }
        $http.post('https://catedraldelafe.herokuapp.com/api/events/store',event)
          .success(function(response){
              $scope.clearScope();
              event.id = response.data.id;
              $scope.events.push(event);
              $location.hash('calendar');
              $anchorScroll();    
          });
    }

    $scope.editEvent = function(){
      console.log('Edited', $scope.newEvent);    
      var event = {
            id : $scope.newEvent.id,
            title: $scope.newEvent.title,
            type: 'info',
            tags: $scope.newEvent.tags,
            description:$scope.newEvent.description,
            startsAt: Date.parse($scope.newEvent.startsAt),
            endsAt: Date.parse($scope.newEvent.endsAt),        
      }
      $http.patch('https://catedraldelafe.herokuapp.com/api/events/update/'+event.id,event)
        .success(function(response){
            var index = $filter('getById')($scope.events, event.id); 
            $scope.events[index] = $scope.newEvent;
            $scope.clearScope();
            $location.hash('calendar');
            $anchorScroll();
        });
    }

    $scope.clearScope = function(){
        $scope.newEvent = {
          title: '',
          description:'',
          tags: '',
          startsAt: '',
          endsAt: '',
          editable: false
        };   
        $scope.eventForm.$setUntouched();
    }

}])

.filter('getById', function(){
  return function(events, id){
    for (var i = 0; i < events.length; i++) {
      if (events[i].id == id){
        return i;
      }
    };
  }
});