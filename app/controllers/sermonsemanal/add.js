'use strict';

angular.module('catedralApp.sermonsemanaladd', ['ngRoute','ui.tinymce','ngFileUpload','ngSanitize'])

.controller('SermonSemanalAddController', ['$scope', 'Upload','$compile','$location',function($scope,Upload,$compile,$location) {

  //$scope.loggedIn = loggedInStatus.getStatus();

  $scope.link = '';
  $scope.v_index = 0;
$scope.initEditor = function(){

};
  $scope.videos = [];
  $scope.news = true;

$scope.settings = {
      action: 'Guardar',
      home: 'sermonsemanal'
  };

$scope.prayer = {
      title :'',
      description :'',
      images :'',
      imagesExist: '',
      edit: false,
      videos: []
  };


$scope.removeItem = function(index){
    $scope.prayer.images.splice(index, 1);
}

  //$scope.link = 'https://www.youtube.com/watch?v=OPmOXJtxxoo';
$scope.limpiar = function(){
  $scope.prayer.title ="";
  $scope.prayer.description ="";
  $scope.prayer.images ="";
};

$scope.submit = function() {
      $scope.uploadFiles($scope.prayer.images);
  };

  $scope.uploadFiles = function (files) {
      Upload.upload({
          url: 'https://catedraldelafe.herokuapp.com/api/posts/store',
          //url: 'http://localhost:8000/api/posts/store',
          data: {'title': $scope.prayer.title, 'description':$scope.prayer.description,'type_id':2,'post_images': files, 'post_videos': $scope.videos},
          headers: {"Content-type": undefined},
      }).then(function (resp) {
          if(resp.status == 201){
                $location.path('/sermonsemanal/view/'+resp.data.data.id);  
          }
          if(resp.data.message){
              //alert(resp.data.message);
              $scope.settings.success = resp.data.message;
              angular.copy({},$scope.prayer);
              $scope.clearVideos();
          }
          console.log(resp);
      }, function (resp) {
          if(resp.data.error == "token_not_provided"){
                alert("El tiempo se sesión ha caducado. Por favor, ingrese de nuevo al sistema");
                $scope.logout();
          }
          if(resp.data.errors){
              $scope.settings.errors = resp.data.errors[0].message;
          }
      });
  }

  $scope.clearVideos = function(){
      $scope.videos = [];
      $("#video_content").empty();
  }

  $scope.deleteVideo = function(event,aux){
      var aux_url = $scope.videos.indexOf($(".video_"+aux+"").data('video').toString());
      if(aux_url !== -1){
          $scope.videos.splice(aux_url,1);
          $(".video_"+aux+"").remove();
      }
  };

  $scope.addVideo = function(){
        var aux = $scope.v_index + 1;
        $scope.v_index = aux;
        $scope.youtubeid = $("#video_link").val().replace("watch?v=", "v/");
        if($scope.youtubeid !== ""){
            $scope.videos.push($scope.youtubeid);
            $scope.video_index = $scope.videos.indexOf($scope.youtubeid);
            var my_video = '<div class="col-md-6 videoClass video_'+aux+'" data-video="'+$scope.youtubeid+'">'+
                                '<div class="embed-responsive embed-responsive-16by9">'+
                                    '<iframe class="embed-responsive-item" src="'+$scope.youtubeid+'" frameborder="0">'+
                                    '</iframe>'+
                                '</div>'+
                                '<div class="hover-btn">'+
                                    '<button type="button" class="btn btn-danger" data-dismiss="alert" ng-click="deleteVideo($event,'+aux+')" ><i class="fa fa-times" ></i></button>'+
                                '</div>'+
                            '</div>';
            angular.element(document.getElementById('video_content')).append($compile(my_video)($scope));

            $("#video_link").val('');    
        }else{
            alert("No se ha ingresado ninguna url de video");
        }
  };
}]);
