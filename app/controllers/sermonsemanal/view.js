'use strict';

angular.module('catedralApp.sermonsemanalview', ['ngRoute','ui.tinymce','ngFileUpload','ngSanitize'])

.controller('SermonSemanalViewController', ['$scope', '$stateParams','DailyPrayerResource',function($scope,$stateParams,DailyPrayerResource) {

    var id = $stateParams.id;
    DailyPrayerResource.get({id:id},function(data){
        $scope.news = data.data;
        $scope.posts = "sermonsemanal";
        $("#newsDescription").append($scope.news.description);
        $scope.band = true;
    })

}]);
