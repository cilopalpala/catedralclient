angular.module('catedralApp.newsDirective', [])

.directive('btnvideos', [function () {
	var valoresInternos = {};
	valoresInternos.scope = {
				link_video : '@link'
			};
	valoresInternos.restrict = 'E';
	valoresInternos.template = "<addtempvideos url_video = {{link_video}}><span class='input-group-addon'><i class='fa fa-plus-square'></i></span></addtempvideos>"
	//console.log('estos son los valores internos ->',valoresInternos);
	return valoresInternos;
}])

.directive('addvideos', [function () {
	return {
		restrict: 'E',
		//template:"<div class='input-group'><input type='text' class='form-control' ng-model='link'><span class='input-group-addon' addtempvideos><i class='fa fa-plus-square'></i></span></div>"
		template: "<div class='input-group'><input type='text' class='form-control' id='video_link' ng-model='video'><btnvideos link='{{video}}'></btnvideos</div>"
	};
}])

.directive('addtempvideos', ['$compile',function ($compile) {
	/*var valoresInternos = {};
	valoresInternos.scope = {
				link_video : '@url_video'
			}*/
	//template: "<span class='input-group-addon' addtempvideos><i class='fa fa-plus-square'></i></span>"
	return function(scope, element, attrs){
		//template: "<span class='input-group-addon'><i class='fa fa-plus-square'></i></span>"

		element.bind("click", function(){
			angular.element(document.getElementById('video_content')).append($compile('<div class="col-md-6 videoClass" >'+
                '<div class="embed-responsive embed-responsive-16by9">'+
                    '<iframe class="embed-responsive-item" src="'+attrs.urlVideo.replace("watch?v=", "v/")+'" frameborder="0">'+
                    '</iframe>'+
                '</div>'+
                '<div class="hover-btn">'+
                    '<button type="button" class="btn btn-danger deleteVideo" ng-click="deleteVideo()" data-dismiss="alert" ><i class="fa fa-times"></i></button>'+
                '</div>'+
            '</div>')(scope));
		});
	};
}])