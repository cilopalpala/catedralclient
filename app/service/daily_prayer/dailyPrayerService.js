'use strict';
angular.module('catedralApp.dailyprayer')

.factory("DailyPrayerResource",function($resource){
            return $resource("https://catedraldelafe.herokuapp.com/api/posts/:id",{id:'@_id'},{
                'query': {method: "POST", isArray:false},
                'update':{method: "PUT", params: {id: "@id"}}
			})
})
